<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\masterModel;

class masterController extends Controller
{
    //
	public function index()
	{
		# code...
		return view("index-work");
	}
	public function register(Request $req)
	{
		# code...
		$response = (new masterModel)->register($req);
		return view($response);
	}

}
