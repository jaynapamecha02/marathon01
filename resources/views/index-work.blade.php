<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Marathon a Sports Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Marathon Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
	<style type="text/css">
		.months p {
	    color: #080808;
	    line-height: 1.5rem;
	    margin: 2em 0 0;
		text-align: left;
	    font-size: 16px;
	}
	
	html {
     scroll-behavior: smooth;
            }
	.months{
		display: none;
		text-align: left;
	}
	.show
	{
		display: block;
	}
	.months ul {
	    margin: 0;
	    padding: 0;
	    padding-left: 56px;
	    font-weight: 700;
	    padding: 13px 50px;
	}
	.months button{
		transition: 0.5s;
	}
	.btn-default:hover, .btn-default:focus, .btn-default.focus, .btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default {
	 color:white;
		background-color:#5bb510;
	}
	.banner-bottom, .features-bottom, .about, .team {
	    padding: 5em 0 2rem 0;
	}
		.faq
		{
			padding:5em 0 4rem 0;
			background:#edf9e0;
		}
		.faq .panel
		{
		    border: none;
		    border-left: 1px solid transparent;
		    border-top-left-radius: 0px;
				background:none;
		}
		.faq .panel-footer {

			border: none;
	    background-color: #cfdac2;

		}
		 .prop {
    border-color: #ccc;
    background: black;
    color: white;
    width: 15%;
    font-size: 15px;
    letter-spacing: 0.4px;
    font-weight: bold;

}
		.faq .active {
			border-bottom-left-radius: 0;
		    border-left: 1px solid #5bb510;
		}
		.w3_agileits_contact_grid_right {
    padding: 60 0 0 60;
}
.w3_agileits_contact_grid_right .center{
padding-top: 10px;
}

.registration .w3_agileits_contact_grid_right {
    padding: 0;
    width: 80%;
    margin: 0 auto;
}
.login-signup-test
{
	padding: 15px 0 10px 0px;	
}
.textfield{
     width: 100%;
     height: 50px;
     outline: none;
     font-weight: bold;
    margin-top: 30px;
    position: unset;
    color: black !important;

}
 label{
 	color: red;
 }
 /*label {
    color: red;
    margin-left: 120px;
}*/	
.modal-content {
    border-radius: 0;
    margin: 120px;
}

.textfield1 {
    width: 100%;
    height: 50px;
    outline: none;
    font-weight: bold;
    margin-top: 30px;
    position: unset;
    color: black !important;
    padding-left: 10px;
    margin-left: 150px;
}
.bt1{
	width: 15%;
   font-size: 15px;
   color: black;
   font-weight: bold;
   font-family: sans-serif;

}

.w3_agileits_contact_grid_right {
    padding: 0px 12em 0px 11em;
}

	</style>

<body>

<!-- banner -->
	<div class="banner">
		<div class="container">
			<div class="header">
				<div class="w3_agile_logo">
					<h1 id="top"><a href="index.html">Marathon<span>keep going</span></a></h1>
				</div>
				<div class="w3_menu">
					<div class="mobile-nav-button">
						<div class="mobile-nav-button__line"></div>
						<div class="mobile-nav-button__line"></div>
						<div class="mobile-nav-button__line"></div>
					</div>
					<nav class="mobile-menu">
						<ul>
							<!-- <li class="active"><a href="index.html">Home</a></li>
							<li><a href="about.html">About</a></li>
							<li><a href="services.html">Services</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="Short Codes">Short Codes <b class="caret"></b></a>
								<ul class="dropdown-menu agile_short_dropdown">
									<li><a href="icons.html">Icons</a></li>
									<li><a href="typography.html">Typography</a></li>
								</ul>
							</li>
							<li><a href="work.html">Our Work</a></li>
							<li><a href="contact.html">Contact</a></li>
							 -->
	<!-- ------------------------------------------------------------>
	<section>
		<li class="active"><a href="#top">HOME</a></li>
		<li><a href="#section1">WHY MARATHON</a></li>
		<li><a href="#section2">Introduction</a></li>
		<li><a href="#section3">Challenges</a></li>
		<!-- <li><a href="#section4">Registration</a></li> -->
		 <li><a href="#section4">Rules & Regulations</a></li>
		<li><a href="#section5">FAQS</a></li>
		<li><a href="#section6">Contact Us</a></li>
       </section>	
	<!-- ----------------------------------------							 -->
						</ul>
					</nav>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="agile_banner_info">
				<h3><span>finishing</span> a marathon is a state of mind that says anything is possible.</h3>
			</div>
			<div class="agileits_w3layouts_more menu__item">
				<a href="#" class="menu__link" data-toggle="modal" data-target="#myregistration">Learn More</a>

			</div><br>
			<div>
		
				<button type="submit"  class="btn btn-default bt1" form="nameform" value="Login" data-toggle="modal" data-target="#mylogin">LOGIN</button>
			</div>
			<div class="w3_agileits_social_media">
				<ul>
					<li class="agileinfo_share">Share On</li>
					<li><a href="#" class="wthree_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#" class="wthree_dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					<li><a href="#" class="wthree_behance"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- choose-us -->
	<section id="section1">
		<div class="choose-us">
			<div class="container">
				<h3 class="w3_agile_header">Why <span>Choose</span> Us</h3>
				<p class="agile_para">Integer laoreet nunc nisl, quis viverra lectus interdum sed.</p>
				<div class="w3ls_banner_bottom_grids">
					<div class="col-md-4 w3ls_choose_us_grid">
						<div class="agileinfo_feature_grid1 agileinfo_service_grid1">
							<div class="col-xs-9 w3_agileits_features_grid1">
								<h4>tempor imperdiet augue</h4>
								<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
							</div>
							<div class="col-xs-3 w3_agileits_features_grid11">
								<div class="wthree_features_grid hi-icon">
									<i class="fa fa-eye" aria-hidden="true"></i>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="agileinfo_feature_grid1 agileinfo_service_grid1">
							<div class="col-xs-9 w3_agileits_features_grid1">
								<h4>vehicula ut ornare</h4>
								<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
							</div>
							<div class="col-xs-3 w3_agileits_features_grid11">
								<div class="wthree_features_grid hi-icon">
									<i class="fa fa-map-pin" aria-hidden="true"></i>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-md-4 w3ls_choose_us_grid">
						<img src="images/11.jpg" alt=" " class="img-responsive" />
					</div>
					<div class="col-md-4 w3ls_choose_us_grid">
						<div class="agileinfo_feature_grid1 agileinfo_service_grid1">
							<div class="col-xs-9 w3_agileits_features_grid1">
								<h4>scelerisque ornare ut</h4>
								<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
							</div>
							<div class="col-xs-3 w3_agileits_features_grid11">
								<div class="wthree_features_grid hi-icon">
									<i class="fa fa-lemon-o" aria-hidden="true"></i>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="agileinfo_feature_grid1 agileinfo_service_grid1">
							<div class="col-xs-9 w3_agileits_features_grid1">
								<h4>viverra lectus ornare</h4>
								<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
							</div>
							<div class="col-xs-3 w3_agileits_features_grid11">
								<div class="wthree_features_grid hi-icon">
									<i class="fa fa-leaf" aria-hidden="true"></i>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		</section>
	<!-- //choose-us -->

	<!-- banner-bottom -->
	<section id="section2">
		<div class="banner-bottom">
			<div class="container">
				<div class="agileits_banner_bottom">
					<h3><span>marathon is an event</span> where everyone is equal and ordinary at the starting line
						but a <i>Legend</i> is born at the finish line</h3>
					<p>Sed sed turpis finibus, lobortis leo sit amet, convallis turpis. Ut suscipit tortor arcu,
						eget condimentum ipsum imperdiet eget. In sit amet pharetra augue. Duis pellentesque accumsan
						nisl. Nam eget placerat nibh. Vestibulum non magna eu dui ullamcorper semper. Lorem
						ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>
	</section>
	<section id="section3">
				
				<div class="w3ls_banner_bottom_grids"> 
				<h3 class="w3_agile_header"><span>CHALLENGES</span></h3>
					<ul class="cbp-ig-grid">
						<li id="3-months">
							<!-- - -------------------------------------------------- -->
							<div class="w3_grid_effect">
								<span class="cbp-ig-icon w3_road"></span>
								<h4 class="cbp-ig-title">3 Months</h4>
								<span class="cbp-ig-category">marathon</span>
							</div>
						</li>

						<li id="6-months">
							<div class="w3_grid_effect">
								<span class="cbp-ig-icon w3_cube"></span>
								<h4 class="cbp-ig-title">6 Months</h4>
								<span class="cbp-ig-category">marathon</span>
							</div>


								</li>
						<li id="9-months">
							<div class="w3_grid_effect">
								<span class="cbp-ig-icon w3_users"></span>
								<h4 class="cbp-ig-title">9 Months </h4>
								<span class="cbp-ig-category">marathon</span>
							</div>

								</li>
						<li id="12-months">
							<div class="w3_grid_effect">
								<span class="cbp-ig-icon w3_ticket"></span>
								<h4 class="cbp-ig-title">12 Months</h4>
								<span class="cbp-ig-category">marathon</span>
							</div>

							<!-- ----------------------------------------------------------------- -->
						</li>
					</ul>
</div>
				<div class="months-con">
					<div class="months" id ="3-months-data">

				
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare scelerisque mi, nec ultrices dui consectetur et. In viverra pharetra mollis. Vestibulum interdum orci vel imperdiet sagittis. Cras libero odio, pellentesque id justo ut, dapibus mattis mi. Sed sagittis faucibus dictum. Cras suscipit nibh massa, sit amet efficitur est lacinia ac. Aenean fringilla, sapien eu tincidunt hendrerit, magna ex euismod sapien, quis elementum nulla odio eget quam.</p>
							<ul class=lead>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>	Integer tincidunt turpis sed quam mollis sodales.</li>
								<li>	Ut consectetur urna porttitor nisi tincidunt scelerisque.</li>
								<li>	Vivamus volutpat eros quis felis tincidunt, nec posuere lorem rutrum.</li>
								<li>	Maecenas convallis erat nec purus laoreet pellentesquee.</li></h4>
						</ul>
						<div class="text-center">
						<button type="submit" class="btn btn-default prop" form="nameform" value="REGISTER HERE" data-toggle="modal" data-target="#myregistration" >REGISTER HERE</button>
					</div>
					</div>
    
					<div class="months" id ="6-months-data">
						666666666666666
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare scelerisque mi, nec ultrices dui consectetur et. In viverra pharetra mollis. Vestibulum interdum orci vel imperdiet sagittis. Cras libero odio, pellentesque id justo ut, dapibus mattis mi. Sed sagittis faucibus dictum. Cras suscipit nibh massa, sit amet efficitur est lacinia ac. Aenean fringilla, sapien eu tincidunt hendrerit, magna ex euismod sapien, quis elementum nulla odio eget quam.</p>
							<ul class=lead>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>	Integer tincidunt turpis sed quam mollis sodales.</li>
								<li>	Ut consectetur urna porttitor nisi tincidunt scelerisque.</li>
								<li>	Vivamus volutpat eros quis felis tincidunt, nec posuere lorem rutrum.</li>
								<li>	Maecenas convallis erat nec purus laoreet pellentesquee.</li></h4>
						</ul>
						<div class="text-center">
						<button type="submit"  class="btn btn-default prop" form="nameform" value="REGISTER HERE" data-toggle="modal" data-target="#myregistration">REGISTER HERE</button>
					</div>
					</div>
					<div class="months" id ="9-months-data">
						999999999
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare scelerisque mi, nec ultrices dui consectetur et. In viverra pharetra mollis. Vestibulum interdum orci vel imperdiet sagittis. Cras libero odio, pellentesque id justo ut, dapibus mattis mi. Sed sagittis faucibus dictum. Cras suscipit nibh massa, sit amet efficitur est lacinia ac. Aenean fringilla, sapien eu tincidunt hendrerit, magna ex euismod sapien, quis elementum nulla odio eget quam.</p>
							<ul class=lead>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>	Integer tincidunt turpis sed quam mollis sodales.</li>
								<li>	Ut consectetur urna porttitor nisi tincidunt scelerisque.</li>
								<li>	Vivamus volutpat eros quis felis tincidunt, nec posuere lorem rutrum.</li>
								<li>	Maecenas convallis erat nec purus laoreet pellentesquee.</li></h4>
						</ul>
						<div class="text-center">
						<button type="submit"  class="btn btn-default prop" form="nameform" value="REGISTER HERE" data-toggle="modal" data-target="#myregistration">REGISTER HERE</button>
					</div>
					</div>
					<div class="months" id ="12-months-data">
						12222222222

							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare scelerisque mi, nec ultrices dui consectetur et. In viverra pharetra mollis. Vestibulum interdum orci vel imperdiet sagittis. Cras libero odio, pellentesque id justo ut, dapibus mattis mi. Sed sagittis faucibus dictum. Cras suscipit nibh massa, sit amet efficitur est lacinia ac. Aenean fringilla, sapien eu tincidunt hendrerit, magna ex euismod sapien, quis elementum nulla odio eget quam.</p>
							<ul class=lead>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>	Integer tincidunt turpis sed quam mollis sodales.</li>
								<li>	Ut consectetur urna porttitor nisi tincidunt scelerisque.</li>
								<li>	Vivamus volutpat eros quis felis tincidunt, nec posuere lorem rutrum.</li>
								<li>	Maecenas convallis erat nec purus laoreet pellentesquee.</li></h4>
						</ul>
						<div class="text-center ">
						<button type="submit"  class="btn btn-default prop" form="nameform" value="REGISTER HERE" data-toggle="modal" data-target="#myregistration">REGISTER HERE</button>
					</div>
					</div>
				</div>
	


					<!-- <div class=" Months"> -->
							<!-- <h4>6 Month</h4>
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare scelerisque mi, nec ultrices dui consectetur et. In viverra pharetra mollis. Vestibulum interdum orci vel imperdiet sagittis. Cras libero odio, pellentesque id justo ut, dapibus mattis mi. Sed sagittis faucibus dictum. Cras suscipit nibh massa, sit amet efficitur est lacinia ac. Aenean fringilla, sapien eu tincidunt hendrerit, magna ex euismod sapien, quis elementum nulla odio eget quam.</p>
							<ul>
								<h4><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>	Integer tincidunt turpis sed quam mollis sodales.</li>
								<li>	Ut consectetur urna porttitor nisi tincidunt scelerisque.</li>
								<li>	Vivamus volutpat eros quis felis tincidunt, nec posuere lorem rutrum.</li>
								<li>	Maecenas convallis erat nec purus laoreet pellentesquee.</li></h4>
						</ul>
						<button type="submit" form="nameform" value="REGISTER HERE">REGISTER HERE</button>
						</div>

				<div class=" Months">
						<h4>9 Month</h4>
						<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare scelerisque mi, nec ultrices dui consectetur et. In viverra pharetra mollis. Vestibulum interdum orci vel imperdiet sagittis. Cras libero odio, pellentesque id justo ut, dapibus mattis mi. Sed sagittis faucibus dictum. Cras suscipit nibh massa, sit amet efficitur est lacinia ac. Aenean fringilla, sapien eu tincidunt hendrerit, magna ex euismod sapien, quis elementum nulla odio eget quam.</p>
						<ul> <h4><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>	Integer tincidunt turpis sed quam mollis sodales.</li>
							<li>	Ut consectetur urna porttitor nisi tincidunt scelerisque.</li>
							<li>	Vivamus volutpat eros quis felis tincidunt, nec posuere lorem rutrum.</li>
							<li>	Maecenas convallis erat nec purus laoreet pellentesquee.</li></h4>
					</ul>
					<button type="submit"  form="nameform" value="REGISTER HERE">REGISTER HERE</button>
					</div>
					</li>

					<div class=" Months">
							<h4>12 Month</h4>
							<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare scelerisque mi, nec ultrices dui consectetur et. In viverra pharetra mollis. Vestibulum interdum orci vel imperdiet sagittis. Cras libero odio, pellentesque id justo ut, dapibus mattis mi. Sed sagittis faucibus dictum. Cras suscipit nibh massa, sit amet efficitur est lacinia ac. Aenean fringilla, sapien eu tincidunt hendrerit, magna ex euismod sapien, quis elementum nulla odio eget quam.</p>
							  <ul>
								  <h4><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
								<li>Integer tincidunt turpis sed quam mollis sodales.</li>
								<li>Ut consectetur urna porttitor nisi tincidunt scelerisque.</li>
								<li>Vivamus volutpat eros quis felis tincidunt, nec posuere lorem rutrum.</li>
								<li>Maecenas convallis erat nec purus laoreet pellentesquee.</li>
								</h4>
						</ul>
						<button type="submit" form="nameform" value="REGISTER HERE">REGISTER HERE</button>
							</div> -->

						</div>
			</div>
		</div>
	<!-- //banner-bottom -->
	</section>
<!-- ---------------------------------------------------------------- -->



<!-- features -->
<section id="section4">
	<div class="features">
		<div class="container">
			<h3 class="w3_agile_header">Our <span>Features</span></h3>
			<p class="agile_para">Integer laoreet nunc nisl, quis viverra lectus interdum sed.</p>
			<div class="agileits_w3layouts_features_grids">
				<div class="col-md-4 agileits_w3layouts_features_gridl">
					<div class="agileinfo_feature_grid1">
						<div class="col-xs-9 w3_agileits_features_grid1">
							<h4>tempor imperdiet augue</h4>
							<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
						</div>
						<div class="col-xs-3 w3_agileits_features_grid11">
							<div class="wthree_features_grid hi-icon">
								<i class="fa fa-eye" aria-hidden="true"></i>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="agileinfo_feature_grid1">
						<div class="col-xs-9 w3_agileits_features_grid1">
							<h4>vehicula ut ornare</h4>
							<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
						</div>
						<div class="col-xs-3 w3_agileits_features_grid11">
							<div class="wthree_features_grid hi-icon">
								<i class="fa fa-map-pin" aria-hidden="true"></i>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="agileinfo_feature_grid1">
						<div class="col-xs-9 w3_agileits_features_grid1">
							<h4>scelerisque ornare ut</h4>
							<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
						</div>
						<div class="col-xs-3 w3_agileits_features_grid11">
							<div class="wthree_features_grid hi-icon">
								<i class="fa fa-lemon-o" aria-hidden="true"></i>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 agileits_w3layouts_features_gridl">
					<div class="agileinfo_feature_grid1">
						<div class="col-xs-9 w3_agileits_features_grid1">
							<h4>viverra lectus interdum</h4>
							<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
						</div>
						<div class="col-xs-3 w3_agileits_features_grid11">
							<div class="wthree_features_grid hi-icon">
								<i class="fa fa-leaf" aria-hidden="true"></i>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="agileinfo_feature_grid1">
						<div class="col-xs-9 w3_agileits_features_grid1">
							<h4>et ligula tristique</h4>
							<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
						</div>
						<div class="col-xs-3 w3_agileits_features_grid11">
							<div class="wthree_features_grid hi-icon">
								<i class="fa fa-heartbeat" aria-hidden="true"></i>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="agileinfo_feature_grid1">
						<div class="col-xs-9 w3_agileits_features_grid1">
							<h4>Donec lectus ultrices </h4>
							<p>Maecenas ipsum magna, vehicula ut scelerisque ornare, porta et ligula tristique.</p>
						</div>
						<div class="col-xs-3 w3_agileits_features_grid11">
							<div class="wthree_features_grid hi-icon">
								<i class="fa fa-hourglass-3" aria-hidden="true"></i>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 agileits_w3layouts_features_gridr">
					<img src="images/1.png" alt=" " class="img-responsive" />
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="agileits_countdown">
				<h2>marathon countdown starts</h2>
				<div class="clock w3agile">
					<div class="column days">
						<div class="timer" id="days"></div>
						<div class="w3ls_text">DAYS</div>
					</div>

					<div class="column">
						<div class="timer" id="hours"></div>
						<div class="w3ls_text">HOURS</div>
					</div>

					<div class="column">
						<div class="timer" id="minutes"></div>
						<div class="w3ls_text">MINUTES</div>
					</div>

					<div class="column">
						<div class="timer" id="seconds"></div>
						<div class="w3ls_text">SECONDS</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- //features -->
<!-- countdown -->
	<script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/moment-timezone-with-data.js"></script>
    <script type="text/javascript" src="js/timer.js"></script>
<!-- //countdown -->
<!-- features-bottom -->
	<!-- <div class="features-bottom">
		<div class="container">
			<h3 class="w3_agile_header">Quick <span>Contact</span></h3>
			<p class="agile_para">Integer laoreet nunc nisl, quis viverra lectus interdum sed.</p>
			<div class="w3l_features_bottom_grids">
				<div class="col-md-4 w3layouts_features_bottom_grid">
					<div class="w3_features_bottom_grid">
						<div class="col-xs-4 w3_features_bottom_gridl">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<div class="col-xs-8 w3_features_bottom_gridr">
							<h4>Email</h4>
							<p><a href="mailto:info@example.com">info@example.com</a></p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 w3layouts_features_bottom_grid">
					<div class="w3_features_bottom_grid">
						<div class="col-xs-4 w3_features_bottom_gridl">
							<i class="fa fa-home" aria-hidden="true"></i>
						</div>
						<div class="col-xs-8 w3_features_bottom_gridr">
							<h4>Address</h4>
							<p>348 Melrose Place, Brazil.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 w3layouts_features_bottom_grid">
					<div class="w3_features_bottom_grid">
						<div class="col-xs-4 w3_features_bottom_gridl">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</div>
						<div class="col-xs-8 w3_features_bottom_gridr">
							<h4>Phone</h4>
							<p>+(000) 123 323 213</p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div> -->
</section>
<!-- //features-bottom -->

<!-- FAQ -->
<section id="section5">
<div class="faq">
		<div class="container">
			<h3 class="w3_agile_header">HOW CAN WE  <span>HELP</span> YOU</h3>
			<p class="agile_para">Integer laoreet nunc nisl, quis viverra lectus interdum sed.</p>
			<div class="w3ls_banner_bottom_grids">
				<div class="col-md-6">
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
					<div class="panel panel-default">
					  <div class="panel-body">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae fringilla mi.
					  </div>
					  <div class="panel-footer">Panel footer</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	</section>
<!-- //FAQ -->




<!-- ---------------------------------------------------------------- -->
<!-- -------------------------------------------------------------------------- -->
<!-- features-bottom -->
<section id="section6">
	<div class="features-bottom">
		<div class="container">
			<h3 class="w3_agile_header">Quick <span>Contact</span></h3>
			<p class="agile_para">Integer laoreet nunc nisl, quis viverra lectus interdum sed.</p>
				<!-- contact -->
	<div class="about">
			<div class="w3_agileits_contact_grids">
					<div class="container">
				<div class="col-md-6 w3_agileits_contact_grid_left">
					<div class="agile_map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.3905851087434!2d-34.90500565012194!3d-8.061582082752993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab18d90992e4ab%3A0x8e83c4afabe39a3a!2sSport+Club+Do+Recife!5e0!3m2!1sen!2sin!4v1478684415917" style="border:0"></iframe>
					</div>
					<div class="agileits_w3layouts_map_pos">
						<div class="agileits_w3layouts_map_pos1">
							<h3>Contact Info</h3>
							<p>348 Melrose Place, 4538 victoria, 4th block avenue, Brazil.</p>
							<ul class="wthree_contact_info_address">
								<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
								<li><i class="fa fa-phone" aria-hidden="true"></i>+(0123) 232 232</li>
							</ul>
							<div class="w3_agile_social_icons w3_agile_social_icons_contact">
								<ul>
									<li><a href="#" class="icon icon-cube agile_facebook"></a></li>
									<li><a href="#" class="icon icon-cube agile_rss"></a></li>
									<li><a href="#" class="icon icon-cube agile_t"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 w3_agileits_contact_grid_right">
					<h3 class="w3_agile_header">Leave a<span> Message</span></h3>
					<p class="agile_para">Integer laoreet nunc nisl, quis viverra lectus interdum sed.</p>
					<form action="#" method="post">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="text" id="input-81" name="Name" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-81">
								<span class="input__label-content input__label-content--ichiro">Your Name</span>
							</label>
						</span>
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="email" id="input-82" name="Email" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-82">
								<span class="input__label-content input__label-content--ichiro">Your Email</span>
							</label>
						</span>
						<textarea name="Message" placeholder="Your message here..." required=""></textarea>
						<input type="submit" value="Submit">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	<!-- contact -->
              <!-- //features-bottom -->
<!-- features-bottom -->


			<!-- <h3 class="w3_agile_header">Quick <span>Contact</span></h3>
			<p class="agile_para">Integer laoreet nunc nisl, quis viverra lectus interdum sed.</p> -->
			<div class="w3l_features_bottom_grids">
				<div class="col-md-4 w3layouts_features_bottom_grid">
					<div class="w3_features_bottom_grid">
						<div class="col-xs-4 w3_features_bottom_gridl">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<div class="col-xs-8 w3_features_bottom_gridr">
							<h4>Email</h4>
							<p><a href="mailto:info@example.com">info@example.com</a></p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 w3layouts_features_bottom_grid">
					<div class="w3_features_bottom_grid">
						<div class="col-xs-4 w3_features_bottom_gridl">
							<i class="fa fa-home" aria-hidden="true"></i>
						</div>
						<div class="col-xs-8 w3_features_bottom_gridr">
							<h4>Address</h4>
							<p>348 Melrose Place, Brazil.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 w3layouts_features_bottom_grid">
					<div class="w3_features_bottom_grid">
						<div class="col-xs-4 w3_features_bottom_gridl">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</div>
						<div class="col-xs-8 w3_features_bottom_gridr">
							<h4>Phone</h4>
							<p>+(000) 123 323 213</p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

		</div>
	<!-- </div> -->
<!-- -------------------------------------------------------------------------- -->
</section>
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="agileits_w3layouts_footer_grids">
				<div class="col-md-3 w3_agileits_footer_grid">
					<h3>About Us</h3>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse.<span>Excepteur sint occaecat cupidatat
						non proident, sunt in culpa qui officia deserunt mollit.</span></p>
				</div>
				<div class="col-md-3 w3_agileits_footer_grid">
					<h3>Contact Info</h3>
					<ul>
						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>348 Melrose Place, 4th block, <span>Brazil.</span></li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+1234 567 567</li>
					</ul>
				</div>
				<div class="col-md-2 w3_agileits_footer_grid w3_agileits_footer_grid1">
					<h3>Navigation</h3>
					<ul>
						<li><a href="about.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>About Us</a></li>
						<li><a href="work.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Our Work</a></li>
						<li><a href="services.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Services</a></li>
						<li><a href="contact.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Mail Us</a></li>
					</ul>
				</div>
				<div class="col-md-4 w3_agileits_footer_grid">
					<h3>Connect With Us</h3>
					<form action="#" method="post">
						<input type="email" name="Email" placeholder="Email" required="">
						<input type="submit" value="Send">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div class="wthree_copy_right">
		<div class="container">
			<p>&copy; 2017 Marathon. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
			<div class="w3_agile_social_icons">
				<ul>
					<li><a href="#" class="icon icon-cube agile_facebook"></a></li>
					<li><a href="#" class="icon icon-cube agile_rss"></a></li>
					<li><a href="#" class="icon icon-cube agile_instagram"></a></li>
					<li><a href="#" class="icon icon-cube agile_t"></a></li>
				</ul>
			</div>
		</div>
	</div>

  
</div>


<div class="registration">

<div class="modal fade" id="myregistration" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REGISTER HERE</h4>
        </div>
        <div class="modal-body">
          
           <div class="w3_agileits_contact_grid_right">
  <!-- <h3 class="w3_agile_header">Register<span> Here</span></h3></div> -->
    <div class="form-group"> 
    	<form action="#" id="signupform" >

    <div class="row">

	 <!-- <form method="post" id="signupform"> -->
		<div class="col-md-6">
				<span>
			<input class="textfield" type="text" name="firstname" placeholder="Your Name" id="firstname" />
			<!-- <label  for="firstname">
								<span>Your Name</span>
							</label> -->
						</span>
			<!-- <input type="submit"  class="btn btn-primary" name="signup"> -->
		</div>
  
      <!-- <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="text" id="firstname" name="firstname" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="firstname">
								<span class="input__label-content input__label-content--ichiro">Your Name</span>
							</label>
						</span>
					</div>
	 -->	<!-- <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="number" id="input-26" name="Age" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-26">
								<span class="input__label-content input__label-content--ichiro">Your Age</span>
							</label>
						</span>
					</div>
				</div> -->
				<div class="col-md-6">
				<span>
			<input class="textfield" type="number" name="age" placeholder="Your Age" id="age" />
		</span>
	</div>
</div>

    <div class="row">
<!--       <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="DOB" id="input-27" name="DOB" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-27">
								<span class="input__label-content input__label-content--ichiro">Your DOB</span>
							</label>
						</span></div>

 -->		<div class="col-md-6">
				<span>
			<input class="textfield" type="DOB" name="DOB" placeholder="Your DOB" id="DOB" />
		</span>
	</div>		
<!--  <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="text" id="input-28" name="Occupation" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-28">
								<span class="input__label-content input__label-content--ichiro">Your Occupation</span>
							</label>
						</span>
</div>
 -->
		<div class="col-md-6">
				<span>
			<input class="textfield" type="Occupation" name="Occupation" placeholder="Your Occupation" id="Occupation" />
		</span>
	</div></div>

   <div class="row">
   
   <!-- <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="text" id="input-29" name="city" placeholder="" required="" />
							<label class="input__label input__label--ichiro" for="input-29">
								<span class="input__label-content input__label-content--ichiro">Your City</span>
							</label>
						</span></div>
	 -->
		<div class="col-md-6">
				<span>
			<input class="textfield" type="text" name="city" placeholder="Your City" id="city" />
		</span>
	</div>
	 	<!-- <div class="col-md-6">				
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="text" id="input-30" name="State" placeholder="" required="" />
							<label class="input__label input__label--ichiro" for="input-30">
								<span class="input__label-content input__label-content--ichiro">Your State </span>
							</label>
						</span>
</div> -->		<div class="col-md-6">
				<span>
			<input class="textfield" type="text" name="State" placeholder="Your State" id="State" />
		</span>
	</div>
</div>
   <div class="row">
      <!-- <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="number" id="input-31" name="number" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-31">
								<span class="input__label-content input__label-content--ichiro">Your Contact</span>
							</label>
						</span></div>
	 -->		<div class="col-md-6">
				<span>
			<input class="textfield" type="number" name="contact" placeholder="Your Contact" id="Contact" />
		</span>
	</div>
	<!--  <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="text" id="input-32" name="Address" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-32">
								<span class="input__label-content input__label-content--ichiro">Your Address </span>
							</label>
						</span>
</div> -->
		<div class="col-md-6">
				<span>
			<input class="textfield" type="text" name="Address" placeholder="Your Address" id="Address" />
		</span>
	</div>
</div>
   <div class="row">
     <!--  <div class="col-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="email" id="input-33" name="email" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-33">
								<span class="input__label-content input__label-content--ichiro">Your Email</span>
							</label>
						</span>
					</div> -->
							<div class="col-md-6">
				<span>
			<input class="textfield" type="email" name="email" placeholder="Your email" id="email" />
		</span>
	</div>
		<!-- <div class="col-md-6">			
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="password" id="input-34" name="password" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-34">
								<span class="input__label-content input__label-content--ichiro">Your Password </span>
							</label>

						</span>

</div> -->
		<div class="col-md-6">
				<span>
			<input class="textfield" type="password" name="password" placeholder="Your password" id="password" />
		</span>
	</div>
</div>
   			<div class="w3_agileits_contact_grid_right center">			
    <input type="submit" value="Submit" name="signup" class="btn btn-primary">
    <div class="login-signup-test text-center">
    	<a href="login.html" id="login"data-toggle="modal" data-target="#mylogin" >Already Registred ?? login</a>
    </div>
</div>
</div>
        </div>

        <!-- <div class="modal-footer"> -->
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <!-- </div> -->
      </div>
      
    </div>
  </div>
  
</div>
	</form>
</div>
<div class="login">
	<div class="modal fade" id="mylogin" role="dialog">
    <div class="modal-dialog">
    
	 <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >&times;</button>
          <h4 class="modal-title">Login Here</h4>
        </div>
        <div class="modal-body">
         <!-- <div class="w3_agileits_contact_grid_right"> -->
  <!-- <h3 class="w3_agile_header">Login<span>Details</span></h3></div> -->
    <div class="form-group"> 
    	<form action="frame2.blade.php" id="loginform" class="loginform" method="post">
                      <!--  <div class="row" class="cols-md-6">
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="email" id="input-33" name="email" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-33">
								<span class="input__label-content input__label-content--ichiro">Your Email</span>
							</label>
						</span>
				</div> -->
				<div class="row">
				<div class="col-md-6">
				<span>
			<input class="textfield1" type="email" name="email" placeholder="Your email" id="email" />
		</span>
	</div>
</div>
						<!-- <div class="row" class="cols-md-6">			
						<span class="input input--ichiro">
							<input class="input__field input__field--ichiro" type="password" id="input-34" name="password" placeholder=" " required="" />
							<label class="input__label input__label--ichiro" for="input-34">
								<span class="input__label-content input__label-content--ichiro">Your Password </span>
							</label>
						</span>
					</div> -->
						<div class="row">
				<div class="col-md-6">
				<span>
			<input class="textfield1" type="password" name="password" placeholder="Your password" id="password" />
		</span>
	</div>
</div>
   			<div class="w3_agileits_contact_grid_right button center">	
    <input type="submit" value="Login">
    <div class="login-signup-test text-center" >
    	<!-- <a href="#" id=registernow >Login</a> -->
    </div>
</div>
</div>

</div>
</form>

<!-- //footer -->
<!-- menu -->
<script>
	$(document).ready(function () {
	  $('.mobile-nav-button').on('click', function() {
	  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(1)" ).toggleClass( "mobile-nav-button__line--1");
	  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(2)" ).toggleClass( "mobile-nav-button__line--2");
	  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(3)" ).toggleClass( "mobile-nav-button__line--3");

	  $('.mobile-menu').toggleClass('mobile-menu--open');
	  return false;
	});
	});
</script>
<!-- //menu -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			$(".faq .panel-footer").slideUp()
			$(".faq .panel").on("click",function()
				{
					$($(this).children()[1]).slideDown()
				});
			$("#3-months").on("click",function () {
				$(".months").removeClass("show");
				$("#3-months-data").addClass("show");

			});

			$("#6-months").on("click",function () {
				$(".months").removeClass("show");

				$("#6-months-data").addClass("show");

			});

			$("#9-months").on("click",function () {
				$(".months").removeClass("show");

				$("#9-months-data").addClass("show");

			});

			$("#12-months").on("click",function () {
				$(".months").removeClass("show");

				$("#12-months-data").addClass("show");

			});
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
				};
			*/

			// $().UItoTop({ easingType: 'easeOutQuart' });

			});
	</script>
<!-- //here ends scrolling icon -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
	<script type="text/javascript">
			



	</script>
<!-- //for bootstrap working -->
 <script type="text/javascript" src="js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript">
		$.validator.setDefaults( {
			submitHandler: function () {
			
				alert(mydata);
			}
		} );

		$( document ).ready( function () {
			$( "#signupform" ).validate( {
				rules: {
					firstname : {
						required: true,
				        minlength: 3
				},
				age: {
                  required: true,
                  number: true,
                 min: 18
      },
              email: {
                  required: true,
                 email: true
      },
              	contact: {
                  required: true,
                  number: true,
                  maxlength: 11
      },
                DOB: {
                	required: true

                },
                password: {
                	required: true,
                	minlength: 8

                },
                Occupation: {
                	required: true


                }

         

					},
					message:
					{
                   firstname: "Please enter your firstname",
				
					},
				});
		});
 
							</script>


	<script type="text/javascript">
		$.validator.setDefaults( {
			submitHandler: function () {
				alert( "submitted!" );
  	           	 var firstname = $('#firstname').val();
  	             
  	               var age= $('#age').val();
  	              
  	               var DOB = $('#DOB').val();
  	              
  	               var Occupation = $('#Occupation').val();
  	              
  	               var city = $('#city').val();
  	            
  	               var State = $('#State').val();
  	             
  	               var Contact = $('#Contact').val();
  	              
  	               var Address = $('#Address').val();
  	               
                   var email = $('#email').val();

  	               var password= $('#password').val();
  	           
  	            mydata={
  	            	"firstname": firstname,
  	            	"age": age,
  	           	    "email": email,
  	           		"password": password,
  	           		"Address": Address,
  	           		"city": city,
  	           		"Contact": Contact,
  	           		"DOB": DOB,
  	           		"Occupation": Occupation,
  	           		"State": State
  	           };


  	           $.ajax({

  	           	url:"/register",
  	           	method:"GET",
  	           	data:mydata,
  	           	success:function (res) {
  	           		console.log("Backend se aaya hun",res);
  	           	},

  	           });
			}
		});

		$( document ).ready( function () {
			$( "#loginform" ).validate( {
				rules: {
		
              email: {
                  required: true,
                 email: true
      },
                password: {
                	required: true,
                	minlength: 8

                }
                

         

					},
					message:
					{
                   firstname: "Please enter your firstname",
				
					},
				});
		});
 
							</script>




</body>
</html>